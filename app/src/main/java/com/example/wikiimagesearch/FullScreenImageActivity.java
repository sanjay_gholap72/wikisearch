package com.example.wikiimagesearch;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

public class FullScreenImageActivity extends AppCompatActivity implements View.OnClickListener {
    ImageView imageView;
    String query,imageURL;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_screen_image);

        Bundle bundle = getIntent().getExtras();
        query = bundle.getString("query");
        imageURL = bundle.getString("imagUrl");

        imageView = findViewById(R.id.img_detailsview);

        Picasso.with(this)
                .load(imageURL)
                .into(imageView);

        imageView.setOnClickListener(this);
    }

    @Override
    public void onClick(View view)
    {
         if(view == imageView)
         {
             Imageback();
         }
    }

    private void Imageback()
    {
        Intent intent = new Intent(FullScreenImageActivity.this,SerchResultActivity.class);
        intent.putExtra("searchQuery",query);
        startActivity(intent);
    }


}
