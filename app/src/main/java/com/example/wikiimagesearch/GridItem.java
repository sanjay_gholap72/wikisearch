package com.example.wikiimagesearch;

public class GridItem
{
    private String image;

    public GridItem()
    {
    }

    public String getImage()
    {
        return image;
    }

    public void setImage(String image)
    {
        this.image = image;
    }
}
