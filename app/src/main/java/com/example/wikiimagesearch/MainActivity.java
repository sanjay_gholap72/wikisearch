package com.example.wikiimagesearch;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.material.snackbar.Snackbar;

public class MainActivity extends AppCompatActivity implements View.OnClickListener
{
    private EditText edtText;
    private Button btnsearch;

    private String searchQuery,url;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        edtText = findViewById(R.id.edt_search);
        btnsearch = findViewById(R.id.btn_search);

        btnsearch.setOnClickListener(this);
    }

    @Override
    public void onClick(View view)
    {
        if(view == btnsearch)
        {
            // btn search pressed
            SearchImage();
        }
    }


    private void SearchImage()
    {
        searchQuery = edtText.getText().toString().trim();

        if(searchQuery.isEmpty())
        {
            Snackbar.make(findViewById(android.R.id.content),"Search Query Can Not Be Empty",Snackbar.LENGTH_LONG).show();
        }
        else
        {
            Intent intent = new Intent(MainActivity.this,SerchResultActivity.class);
            intent.putExtra("searchQuery",searchQuery);
            startActivity(intent);
        }
    }

    @Override
    public void onBackPressed()
    {
        new AlertDialog.Builder(this)
                    .setTitle("Really Exit?")
                    .setMessage("Are you sure,you want to exit?")
                    .setNegativeButton(android.R.string.no, null)
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener()
                    {
                        public void onClick(DialogInterface arg0, int arg1)
                        {
                            Intent intent = new Intent(Intent.ACTION_MAIN);
                            intent.addCategory(Intent.CATEGORY_HOME);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
                            intent.putExtra("EXIT", true);
                            startActivity(intent);
                            System.exit(0);
                            finish();
                        }
                    }).create().show();
        }

}
