package com.example.wikiimagesearch;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Iterator;

public class SerchResultActivity extends AppCompatActivity
{
    private String TAG = "SerchResultActivity";
    private String searchQuery,query;
    private GridView mGridView;

    private GridViewAdapter mGridAdapter;
    private ArrayList<GridItem> mGridData;
    GridItem item;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_serch_result);

        Bundle bundle = getIntent().getExtras();
        query = bundle.getString("searchQuery");

        mGridView = (GridView) findViewById(R.id.gridview);
        mGridData = new ArrayList<>();
        mGridAdapter = new GridViewAdapter(this, R.layout.grid_item_layout, mGridData);

        mGridView.setAdapter(mGridAdapter);

        searchQuery = "https://en.wikipedia.org/w/api.php?action=query&prop=pageimages&format=json&piprop=thumbnail&pithumbsize=200&pilimit=50&generator=prefixsearch&gpssearch="+query;
        Log.wtf("searchQuery",searchQuery);
        new AsyncHttpTask().execute(searchQuery);

        mGridView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l)
            {
                String imagURL  = String.valueOf(mGridData.get(i).getImage());
                Intent intent = new Intent(SerchResultActivity.this,FullScreenImageActivity.class);
                intent.putExtra("imagUrl",imagURL);
                intent.putExtra("query",query);
                startActivity(intent);

            }
        });

    }

        class AsyncHttpTask extends AsyncTask<String, Void, Integer>
        {

            @Override
            protected Integer doInBackground(String... params)
            {

                Integer result = 0;
                try {
                    // Create Apache HttpClient
                    HttpClient httpclient = new DefaultHttpClient();
                    HttpResponse httpResponse = httpclient.execute(new HttpGet(params[0]));
                    int statusCode = httpResponse.getStatusLine().getStatusCode();

                    // 200 represents HTTP OK
                    if (statusCode == 200)
                    {
                        String response = streamToString(httpResponse.getEntity().getContent());

                        parseResult(response);
                        result = 1; // Successful
                    }
                    else
                    {
                        result = 0; //"Failed
                    }
                } catch (Exception e)
                {
                    Log.d(TAG, e.getLocalizedMessage());
                }
                return result;
            }

            String streamToString(InputStream stream) throws IOException
            {
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(stream));
                String line;
                String result = "";
                while ((line = bufferedReader.readLine()) != null)
                {
                    result += line;
                }

                // Close stream
                if (null != stream)
                {
                    stream.close();
                }
                return result;
            }

            @Override
            protected void onPostExecute(Integer result)
            {

                if (result == 1)
                {
                    mGridAdapter.setGridData(mGridData);
                }
                else
                {
                    Toast.makeText(SerchResultActivity.this, "Failed to fetch data!", Toast.LENGTH_SHORT).show();
                }

            }
        }

        private void parseResult(String result)
        {

            try
            {


                JSONObject jsonObject = new JSONObject(result);
                JSONObject query = jsonObject.getJSONObject("query");

                JSONObject pages = query.getJSONObject("pages");


                Iterator keys = pages.keys();

                while(keys.hasNext())
                {

                    String currentDynamicKey = (String)keys.next();
                    Log.wtf("currentDynamicKey",currentDynamicKey);

                     JSONObject keyData = pages.getJSONObject(currentDynamicKey);
                     JSONObject thumbnail = keyData.getJSONObject("thumbnail");

                     String image_url = thumbnail.getString("source");
                     item = new GridItem();
                     item.setImage(image_url);
                     mGridData.add(item);
                }


            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }
        }

    }







